Список задач на третью неделю: с 12:00 29 марта по 20:00 2 апреля

1. [Think Python 2e](../thinkpython2.pdf)

Глава 8 <br>
Упражнение 8.13. <br>
Упражнение 8.1. Результат: выберите топ 10 полезных методов из библиотеки. Напишите для них примеры использования. Примеры использования - это любые константы string. Инициализируйте их в начале файла .py. <br>
У вас должен получиться .py файл, в котором есть функция main. Она вызывает 10 методов (из библиотеки python или из вашей обертки). Каждый из методов работает с одной из констант-примеров, которые вы инициализировали в начале файла. <br>
Упражнения 8.2, 8.3 <br>
Упражнение 8.4. Результат: .md файл, в котором вы одним коротким абзацем описываете, что конкретно делает функция. Комментарии пишите на английском. Представьте, что вы объясняете работу функции менеджеру, который не умеет читать код, но хочет понять, что код делает. <br>

Для разработчиков: <br>
Упражнение 8.5. <br>
Упражнение 8.6. Напишите функцию, которая принимает на вход string и любой параметр, и зашифрует его по вашим правилам, используя параметр. Правила шифровки и примеры укажите в README.md файле. <br>
'my_encrypt(string, param)'
Упражнение 8.7. Напишите функцию, которая принимает на вход зашифрованную по вашим правилам строку и расшифровывает ее.<br>
'my_decrypt(string)'

Для разработчиков: <br>
Глава 9. Case study: word play <br>
Повторите код из главы 9, соберите его в один файл word_play.py. Не копируйте код, повторите его. К каждому методу напишите docstring. Улучшите код, где видите, что можно сделать лучше. <br>
Упражнения 9.7. - 9.9. Если вы написали креативное решение, не такое, как в референсах, то напишите вашему менеджеру по обучению, чтоб мы проверили и оценили ваше решение отдельно.

Глава 10 <br>
Упражнение 10.15. <br>
Упражнения 10.1 - 10.10<br>
Для разработчиков: Упражнения 10.11 - 10.12<br>

Названия .py файлов: exerciseY-Z, например, для главы 2 и упражнения 2.2 назовите файл exercise2-2.py  
Результат выполнения: .py файлы для задач, и .md файлы для вопросов  

Ответы на вопросы пишите на англ. языке. <br>
Код должен соответствовать DOD.

2. [Изучаем SQL](Alan_byuli-Izuchaem_SQL-Simvol-Plyus_2007.pdf) или [Learning SQL](Alan_Beaulieu-Learning_SQL-EN.pdf)

Глава 7. Data Generation, Conversion, and Manipulation
Повторить все запросы из главы 7. <br>
Упражнения 7.1 - 7.3

Результат выполнения: код из упражнений 7.1 - 7.3 в одном .sql файле task7.sql, скриншоты с результатами выполненных заданий (1 задание - 1 скриншот), код из главы 7 (все примеры), скриншоты с результатами запросов из главы (сколько получится, можно несколько запросов с результатами на одном скриншоте.) <br>
В README файл к заданию добавьте подробное объяснение, какой скриншот содержит какую информацию. Сделайте 1 README к главе 7, и 1 README к заданиям 7.1 - 7.3






Будет дополнено
