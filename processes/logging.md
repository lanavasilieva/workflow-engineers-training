## Logging Rules

Please follow [the official logging tutorial](https://docs.python.org/3/howto/logging.html).<br>
Use Logging python library. If you're using other library to handle logs, please justify your choice in a README file. <br>

Log only the following level of severity:<br>
`INFO` <br>
`WARNING` <br>
`ERROR` <br>
`CRITICAL` <br>

Use [Exceptions](https://docs.python.org/3/tutorial/errors.html) to report an error regarding a particular runtime event. For example, use exceptions handling for errors from `Handle error conditions and edge cases` point from [DOD](definition-of-done.md).

Log to a file, not to the console. <br>
Name the file `logger.log`. Do not include a file with your logs to your git repository! <br>

Additional reading: <br>
[Logging Cookbook](https://docs.python.org/3/howto/logging-cookbook.html#logging-cookbook)

