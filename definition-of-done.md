# WFE Definition of Done

### OVERVIEW: Definition Of Done for any programming task (python, sql)
| Item |
|---|
| Create a Task in Redmine. Set OMP and Due Date |
| Create a separate Branch |
| Add README.md. Add DOD. Add questions and answers to requirements. Add a link to a ticket in Redmine.|
| Add detailed description of development steps, inputs and outputs to Docstring |
| Develop according to Functional Requirements|
| The code complies with coding style guides and style checks are green|
| Handle error conditions and edge cases |
| Add necessary logging for potential troubleshooting |
| Launch the code locally more than once, check if it produces the expected results |
| Include the test data necessary for manual testing |
| Update README, add information for launching |
| Code review is passed and code is merged|







### Detailed Description
| Item | Description|
|---|---|
| Create a Task in Redmine. Set OMP and Due Date |В [проекте в Редмайне](https://academy-redmine.jarvis.syberry.net/projects/workflow-engineers-training/issues) вы создали Issue для этой задачи, назвали ее [по требованиям](tasks-requirements.md). Вы прочитали требования к задаче и запланировали время работы над задачей, указали его в параметрах О, М, Р. Вы поставили себе дедлайн и указали его в Due Date.|
| Create a separate Branch |Вы сделали отдельную ветку с задачей, назвали ее [по требованиям](guidelines/git-process.md)|
| Add README.md. Add DOD. Add questions and answers to requirements. Add a link to a ticket in Redmine.|Вы сделали README файл. Добавили туда DOD чеклист. Вы прочитали требования к задаче и подготовили вопросы по ней. Вы записали эти вопросы в README. Если вопросов нет, вы написали "Нет вопросов к задаче". Вы обсудили вопросы с тренером или приняли собственные решения. Вы записали ответы на вопросы. Вы добавили ссылку на задачу в редмайне.|
| Add detailed description of development steps, inputs and outputs to Docstring |Вы создали .py или .sql файл. Вы добавили документацию в файл. В документации описано, как вы планируете реализовать задание. Там есть конкретные шаги, описанные на английском, очень простым языком. В Python файлах вы описали весь файл и отдельно - каждый класс и функцию. Минимум информации - это inputs и outputs.|
| Develop according to Functional Requirements|Вы запрограммировали решение. Вы знаете, что решение покрывает весь функционал, описанный в задании.|
| The code complies with coding style guides and style checks are green|Вы установили себе автоматические проверки стиля. В коде нет ошибок по стилю.|
| Handle error conditions and edge cases |Вы продумали, как ваша программа будет обрабатывать ошибки и краевые случаи и запрограммировали обработку. Ваша программа работает стабильно с ошибками и краевыми случаями.|
| Add necessary logging for potential troubleshooting |[Вы записываете логи](processes/logging.md).|
| Launch the code locally more than once, check if it produces the expected results |Вы запустили ваш код как  минимум один раз и он сработал так, как вы предполагали. |
| Include the test data necessary for manual testing |Вы добавили тестовые данные для мануального тестирования. Это данные, с которыми вашу программу можно запустить и она отработает как описано в требования. Запускать программу будет ментор и QA инженер без вашего участия. |
| Update README, add information for launching |Вы добавили в README подробную пошаговую инструкцию о том, как запустить вашу программу, как использовать тестовые данные, какой будет результат использования. Вы обновили README, если во время разработки вы обнаружили что-то новое.|
| Create Merge Request to dev |Вы создали merge request из вашей ветки в dev ветку, assignee - svetlana vasilieva. Это - последний шаг выполнения задания. Тем, что вы сделали мерж реквест, вы подтверждаете, что вы выполнили задание на 100%. Ваш дедлайн - это последний день, когда вы можете сделать merge request.|
| Code review is passed and code is merged |Ваш ментор проверил ваш код и вмержил его в dev. Этим ментор подтверждает, что ваше задание выполнено.|


### Definition Of Done Checklist
| Item | Is confirmed? (Y/N) |
|---|---|
| Create a Task in Redmine. Set OMP and Due Date ||
| Create a separate Branch ||
| Add README.md. Add DOD. Add questions and answers to requirements. Add a link to a ticket in Redmine.||
| Add detailed description of development steps, inputs and outputs to Docstring ||
| Develop according to Functional Requirements||
| The code complies with coding style guides and style checks are green||
| Handle error conditions and edge cases ||
| Add necessary logging for potential troubleshooting ||
| Launch the code locally more than once, check if it produces the expected results ||
| Include the test data necessary for manual testing ||
| Update README, add information for launching ||
| Code review is passed and code is merged||

