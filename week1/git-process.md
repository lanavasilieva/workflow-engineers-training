# git Process

Мы следуем [github flow](https://guides.github.com/introduction/flow/). <br>
[Документация](https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/github-flow) <br>
Вы `ОБЯЗАНЫ` следовать github flow во время работы с гитом.

1. Создайте ветку main и dev
2. Делайте каждую задачу в отдельной ветке. Ветку по задачам начинайте от ветки dev
2. Называйте ветку taskYYYXXX, где YYY - это код предмета, а XXX - это номер задачи. Если в номере задачи есть точка, поменяйте ее на дефис -
Коды предметов:   
Python - PY  
MySQL - MYSQL  
.CSV - CSV  
YAML - YAML  
.md - MD
Например, для упражнения 2.2 из книжки по пайтону вам надо сделать ветку taskPY2-2
3. Задача считается выполненной, если вы сделали merge request ветки с задачей в ветку dev <br>
Assignee реквеста: lanavasilieva
