[Python Code Documentation PEP 257](https://www.python.org/dev/peps/pep-0257/#:~:text=A%20docstring%20is%20a%20string,%2C%20class%2C%20or%20method%20definition.&text=A%20package%20may%20be%20documented,may%20also%20act%20as%20documentation.)

Write a Docstring to the whole file and to each function and class except main().