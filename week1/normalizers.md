[Normalizing Data - Lesson](https://programminghistorian.org/en/lessons/normalizing-data). <br>
Follow the tutorial <br>
Branch name: taskPY-NDL

[Chapter 7: Regular Expressions](https://web.archive.org/web/20180416143856/http://www.diveintopython.net/regular_expressions/index.html)<br>
Complete the Case Studies: Streed Address (Branch name: taskPYCS-SA), Case Study: Roman Numbers (Branch name: taskPYCS-RN), Case Study: Parsing Phone Numbers (Branch name: taskPYCS-PPN)
