[Dataset: Heart Disease](https://www.kaggle.com/johnsmith88/heart-disease-dataset)<br>

Create YAML file heart-disease.yaml containing a structure for this csv file. <br>
Create 3 MySQL INSERT requests with examples from this csv file. <br>

Branch names: taskYAML-1, taskMYQSL-1
