# Workflow Engineers Training Knowledge Base


## Стандарты кода
[Стандарт кода для python](https://www.python.org/dev/peps/pep-0008/)  
Установите проверку своего кода по РЕР8. [Включите PEP8 inspections](https://www.jetbrains.com/help/pycharm/tutorial-code-quality-assistance-tips-and-tricks.html#a4494337)  

[Стандарты для YAML](https://github.com/flathub/flathub/wiki/YAML-Style-Guide)<br>

[Стандарт коммитов в гите](engineering_version-control_commit-style-guide.md)     
Все коммиты в ваших репозиториях обязаны соответствовать этому стандарту.   

## Week 1

1. Создайте приватный репозиторий в своем неймспейсе. Назовите его workflow-engineer-training-name-surname <br>  
Посмотрите лекцию [MIT Version Control (Git)](https://missing.csail.mit.edu/2020/version-control/)  <br>
Все задания складывайте в свой репозиторий. <br>
Добавьте lanavasilieva в ваш репозиторий (developer role).

2. [Think Python 2e](thinkpython2.pdf)
Глава 1: The Way of the Program  <br>
Упражнение Exercise 1.9 (1.1, 1.2)  <br>
Глава 2: Variables, Expressions, Statements  <br>
Упражнение Exercise 2.10 (2.1, 2.2)  <br>
Глава 3: Functions  <br>
Упражнение Exercise 3.14 (3.1 - 3.3)  <br>

Названия .py файлов: exerciseY-Z, например, для главы 2 и упражнения 2.2 назовите файл exercise2-2.py  
Результат выполнения: .py файлы для задач, и .md файлы для вопросов  

Ответы на вопросы пишите на англ. языке. <br>
Код должен соответствовать DOD.

3. [Изучаем SQL](Alan_byuli-Izuchaem_SQL-Simvol-Plyus_2007.pdf) или [Learning SQL](Alan_Beaulieu-Learning_SQL-EN.pdf)
Глава 1: A Little Background  
Глава 2: Creating and Populating a Database  
Упражнения:  
стр. 15 Creating a MySQL Database  
стр. 17 Using the mysql Command-Line Tool  
В главе 2 повторять те же запросы, что и в книжке, начиная со страницы 25 - Table Creation  <br>
стр. 38 The Bank Schema  <br>
Результат выполнения - сделать скриншоты со всеми командами из книжки с результатами их выполнения.<br>
Скриншоты назвать mysql-practice-w1-X.png и добавить в своей репозиторий в папку mysql-training

4. [Yaml Documentation](spec.pdf)  
Глава 2: Preview и Глава 3: Processing YAML Information

5. [Yaml на русском](https://coderlessons.com/tutorials/raznoe/vyuchi-yaml/yaml-vvedenie)
Главы: введение, основы, отступы и разделение, комментарии

6. [Туториал по PyYaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
Выполните все шаги installation и туториала 




