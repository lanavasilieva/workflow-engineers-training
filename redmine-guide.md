# Redmine Frequently Asked Questions
If you don't see your question here, write us in Discord

- [What is Redmine and What are the main features?](redmine-guideline.md#redmine-overview)
- [How do I report time?](redmine-guideline.md#time-reporting-guide)
- [What time formats are used in Redmine?](redmine-guideline.md#time-formats-in-redmine)
- [How do I make a time estimation?](redmine-guideline.md#time-estimation-guide)
- [What should I do if the estimate I provided is incorrect?](redmine-guideline.md#time-reestimation)
- [What does O, M, P mean?](redmine-guideline.md#o-m-p)
- [What should I do if I forgot to report my time?](redmine-guideline#workload-reporting-issues)

## Redmine Overview
Redmine is a Syberry Issue Tracking Tool.
Here you'll find [a presentation describing the main features and principles of Redmine in Syberry](files/redmine-presentation.pdf)

## How do I report time?
Choose "Log Time" in your issue
![time-reporting-log-time.png](files/time-reporting-log-time.png) <br>
Provide time report and comments <br>
![time-estimation-comment.png](files/time-estimation-comment.png)<br>
After clicking "Create" you'll see the confirmation page and your time report<br>
![time-estimation-done.png](files/time-estimation-done.png)<br>
15 minutes after time reporting, you may edit it. Click "Edit" to do so<br>
![time-reporting-edit.png](files/time-reporting-edit.png)<br>

## Time formats in Redmine
Please be careful with time formats in Redmine! 1,2 and 1:20 mean different things! <br>
1:30 means 1 hour 30 minutes <br>
1h30m means 1 hour 30 minutes <br>
1,5 means 1 hour 30 minutes <br>

## Time Estimation Guide
When you've got a new Issue, you have to provide a time estimation. Time estimation for an Issue should be based on your experience and expert opinion and should answer one question "How much time am I going to spend doing this task?"

Fill in three fields: O, M, P<br>
You have to provide Time estimation the same day you've got your issue<br>
To estimate the issue, click Edit in Issue menu<br>
![issue-edit.png](files/issue-edit.png) <br>
Find in O, M, P fields. Please note: O < M < P <br>
![issue-o-m-p.png](files/issue-o-m-p.png)<br>
Enter your time estimation. Time in the Estimated time field will generate automatically. Don't forget to click the "Submit" button<br>
![issue-time-set-submit.png](files/issue-time-set-submit.png)<br>

## Time Reestimation
If, after working some time on the issue, you realized that you need more time to resolve it, you have to make time reestimation and provide comments, why you're doing so<br>
![issue-reestimation.png](files/issue-reestimation.png)

## O, M, P
- O - Optimistic Time Estimation - meaning "If everything goes ideally, I am going to work on this issue O hours or less. This is my fastest time" <br>
- M - Most Likely Time Estimation - meaning "My previous experience shows that I am going to work on this issue M hours or less. This is my regular time" <br>
- P - Pessimistic Time estimation - meaning "Considering all risks, unknown unknowns, and dependencies (if you are not sure how to implement this issue, or you have never implemented issues like this), I am going to do this task in P hours or less. This is my slowest time" <br>
Please note: O < M < P <br>

## Workload Reporting Issues
Redmine allows you to report yesterday's hours. Click on the Calendar icon on Date field and choose yesterday's date. <br>
![yesterday-report.png](files/yesterday-report.png) <br>
You can only report your time starting with the latest date. <br>
If you have any problems regarding time reporting, please contact your Manager <br>
